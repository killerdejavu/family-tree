## Dependencies
Only python3

## The way to run code
python3 -m family_tree.runner


## The way to run tests
python3 -m unittest discover -s family_tree/tests -v


## Some Assumptions
- Simpler monogamous Male - Female Relationships
- Once a spouse is added, you cant change it (No divorces!)
- My children are my spouses children as well

## Things which can be better
- More test cases. Right now tests are more indicative then exhaustive
- Better process engine. Make it more generic, readable and can accept any object instead of just a list
- Better naming nomenclature for relationship_steps/base_steps
- Depending upon the goal:
    - if the number of nodes are going to be too high, we should be pre computing relationships on any node change
    - if the number of nodes are low in compare to number of relationships, we can calculate data on the fly as done now
 