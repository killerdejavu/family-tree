from enum import Enum

from family_tree.relationship_steps import RelationshipSteps


class Relationships(Enum):
    PARENTS = {
        'display_name': 'PARENT',
        'steps_to_reach': RelationshipSteps.parents
    }

    FATHER = {
        'display_name': 'FATHER',
        'steps_to_reach': RelationshipSteps.father
    }

    MOTHER = {
        'display_name': 'MOTHER',
        'steps_to_reach': RelationshipSteps.mother
    }

    SONS = {
        'display_name': 'SON',
        'steps_to_reach': RelationshipSteps.sons
    }

    DAUGHTERS = {
        'display_name': 'DAUGHTER',
        'steps_to_reach': RelationshipSteps.daughters
    }

    CHILDREN = {
        'display_name': 'CHILD',
        'steps_to_reach': RelationshipSteps.children
    }

    BROTHERS = {
        'display_name': 'BROTHER',
        'steps_to_reach': RelationshipSteps.brothers
    }

    SISTERS = {
        'display_name': 'SISTER',
        'steps_to_reach': RelationshipSteps.sisters
    }

    SIBLINGS = {
        'display_name': 'SIBLING',
        'steps_to_reach': RelationshipSteps.siblings
    }

    GRAND_DAUGHTERS = {
        'display_name': 'GRAND DAUGHTER',
        'steps_to_reach': RelationshipSteps.grand_daughters
    }

    COUSINS = {
        'display_name': 'COUSIN',
        'steps_to_reach': RelationshipSteps.cousins
    }

    BROTHER_IN_LAWS = {
        'display_name': 'BROTHER IN LAW',
        'steps_to_reach': RelationshipSteps.brother_in_laws
    }

    SISTER_IN_LAWS = {
        'display_name': 'SISTER IN LAW',
        'steps_to_reach': RelationshipSteps.sister_in_laws
    }

    MATERNAL_AUNTS = {
        'display_name': 'MATERNAL AUNT',
        'steps_to_reach': RelationshipSteps.maternal_aunts
    }

    MATERNAL_UNCLE = {
        'display_name': 'MATERNAL UNCLE',
        'steps_to_reach': RelationshipSteps.maternal_uncles
    }

    PATERNAL_AUNTS = {
        'display_name': 'PATERNAL AUNT',
        'steps_to_reach': RelationshipSteps.paternal_aunts
    }

    PATERNAL_UNCLES = {
        'display_name': 'PATERNAL UNCLE',
        'steps_to_reach': RelationshipSteps.paternal_uncles
    }



