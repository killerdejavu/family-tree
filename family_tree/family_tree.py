from family_tree import process_engine
from family_tree.person import Gender
from family_tree.relationships import Relationships


class FamilyTree(object):
    def __init__(self, root_person):
        self.root_person = root_person
        self._mother_with_most_girl_child = []
        self._number_of_girl_child = 0

    @staticmethod
    def find_persons_from_relationship(person, relationship):
        return process_engine.process(data=[person],
                                      steps=relationship.value.get(
                                          'steps_to_reach'))

    def find_mother_with_most_girl_child(self):

        def traverse(root_person):
            if root_person.gender == Gender.MALE:
                mothers = root_person.spouse
            else:
                mothers = [root_person]
            for mother in mothers:
                if mother.gender == Gender.FEMALE:
                    daughters = self.find_persons_from_relationship(mother,
                                                                    Relationships.DAUGHTERS)
                    if len(daughters) == self._number_of_girl_child:
                        self._mother_with_most_girl_child.append(mother)

                    if len(daughters) > self._number_of_girl_child:
                        self._number_of_girl_child = len(daughters)
                        self._mother_with_most_girl_child = [mother]

            for person in root_person.children:
                traverse(person)

        traverse(self.root_person)
        return self._mother_with_most_girl_child

    def find_relationship_between(self, person_1, person_2):

        for relationship in list(Relationships):
            results = self.find_persons_from_relationship(person_1, relationship)
            if person_2 in results:
                return relationship.value.get('display_name')

            # results = self.find_persons_from_relationship(person_2, relationship)
            # if person_1 in results:
            #     print("{} is {} of {}".format(person_1,
            #                                   relationship.value.get('display_name'),
            #                                   person_2))
