import unittest
from collections import Counter

from family_tree import process_engine
from family_tree.person import Person, Gender
from family_tree.relationship_steps import BaseSteps


class TestProcessEngine(unittest.TestCase):

    def test_all_handle_functions_present(self):
        for base_test in list(BaseSteps):
            assert getattr(process_engine, "handle_{}".format(base_test.value)) is not None

    def test_handle_children(self):
        result = process_engine.handle_children([self.king_shan])
        assert Counter(result) == Counter([self.ish, self.chit, self.vich, self.satya])

    def test_handle_male(self):
        result = process_engine.handle_male([self.king_shan, self.vich, self.satya])
        assert Counter(result) == Counter([self.king_shan, self.vich])

    def test_handle_female(self):
        result = process_engine.handle_female([self.king_shan, self.vich, self.satya])
        assert Counter(result) == Counter([self.satya])

    def test_handle_parents(self):
        result = process_engine.handle_parents([self.vich, self.satvy])
        assert Counter(result) == Counter([self.king_shan, self.queen_agna, self.vyan, self.satya])

    def test_handle_spouse(self):
        result = process_engine.handle_spouse([self.vich, self.satya])
        assert Counter(result) == Counter([self.lika, self.vyan])

    def setUp(self):
        self.king_shan = Person("King Shan", Gender.MALE)
        self.queen_agna = Person("Queen Agna", Gender.FEMALE)

        self.king_shan.add_spouse(self.queen_agna)

        self.ish = Person("Ish", Gender.MALE)
        self.chit = Person("Chit", Gender.MALE)
        self.vich = Person("Vich", Gender.MALE)
        self.satya = Person("Satya", Gender.FEMALE)

        self.king_shan.add_child(self.ish)
        self.king_shan.add_child(self.chit)
        self.king_shan.add_child(self.vich)
        self.king_shan.add_child(self.satya)

        self.ambi = Person("Ambi", Gender.FEMALE)
        self.chit.add_spouse(self.ambi)

        self.lika = Person("Lika", Gender.FEMALE)
        self.vich.add_spouse(self.lika)

        self.vyan = Person("Vyan", Gender.MALE)
        self.satya.add_spouse(self.vyan)

        self.drita = Person("Drita", Gender.MALE)
        self.jaya = Person("Jaya", Gender.FEMALE)
        self.vrita = Person("Vrita", Gender.MALE)
        self.jata = Person("Jata", Gender.MALE)
        self.driya = Person("Driya", Gender.FEMALE)
        self.mnu = Person("Mnu", Gender.MALE)

        self.chit.add_child(self.drita)
        self.chit.add_child(self.vrita)
        self.drita.add_spouse(self.jaya)
        self.drita.add_child(self.jata)
        self.drita.add_child(self.driya)
        self.driya.add_spouse(self.mnu)

        self.vila = Person("Vila", Gender.MALE)
        self.jnki = Person("Jnki", Gender.FEMALE)
        self.chika = Person("Chika", Gender.FEMALE)
        self.kpila = Person("Kpila", Gender.MALE)
        self.lavnya = Person("Lavnya", Gender.FEMALE)
        self.gru = Person("Gru", Gender.MALE)

        self.vich.add_child(self.vila)
        self.vich.add_child(self.chika)
        self.vila.add_spouse(self.jnki)
        self.chika.add_spouse(self.kpila)
        self.vila.add_child(self.lavnya)
        self.lavnya.add_spouse(self.gru)

        self.satvy = Person("Satvy", Gender.FEMALE)
        self.asva = Person("Asva", Gender.MALE)
        self.savya = Person("Savya", Gender.MALE)
        self.krpi = Person("Krpi", Gender.FEMALE)
        self.saayan = Person("Saayan", Gender.MALE)
        self.mina = Person("Mina", Gender.FEMALE)
        self.kriya = Person("Kriya", Gender.MALE)
        self.misa = Person("Misa", Gender.MALE)

        self.satya.add_child(self.satvy)
        self.satya.add_child(self.savya)
        self.satya.add_child(self.saayan)
        self.satvy.add_spouse(self.asva)
        self.savya.add_spouse(self.krpi)
        self.saayan.add_spouse(self.mina)
        self.savya.add_child(self.kriya)
        self.saayan.add_child(self.misa)