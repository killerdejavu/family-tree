import unittest
from collections import Counter

from family_tree.person import Person, Gender


class TestPerson(unittest.TestCase):

    def test_create_person(self):
        per = Person("test", gender=Gender.MALE)

        assert per is not None
        assert per.name == "test"
        assert per.gender == Gender.MALE
        assert per.children == []
        assert per.parents == []
        assert per.spouse == []

    def test_add_spouse(self):
        per = Person("per", gender=Gender.MALE)
        peri = Person("peri", gender=Gender.FEMALE)
        per.add_spouse(peri)

        assert per.spouse == [peri]
        assert peri.spouse == [per]

    def test_add_child(self):
        per = Person("per", gender=Gender.MALE)
        peri = Person("peri", gender=Gender.FEMALE)
        per.add_spouse(peri)

        c1 = Person("c1", gender=Gender.MALE)
        c2 = Person("c2", gender=Gender.FEMALE)
        c3 = Person("c3", gender=Gender.FEMALE)

        per.add_child(c1)
        per.add_child(c2)
        per.add_child(c3)

        assert Counter(per.children) == Counter([c1, c2, c3])
        assert Counter(peri.children) == Counter([c1, c2, c3])
