import unittest
from collections import Counter

from family_tree.family_tree import FamilyTree
from family_tree.person import Person, Gender
from family_tree.relationships import Relationships


class TestFamilyTree(unittest.TestCase):

    def test_create_family_tree(self):
        per = Person("per", Gender.MALE)

        famiy_tree = FamilyTree(root_person=per)

        assert famiy_tree is not None
        assert famiy_tree.root_person == per

    def test_find_mother_with_most_girl_child(self):
        family_tree = FamilyTree(self.king_shan)

        results = family_tree.find_mother_with_most_girl_child()
        self.assertEqual(Counter(results), Counter([self.queen_agna, self.jaya, self.lika, self.jnki, self.satya]))

        drini = Person("Drini", gender=Gender.FEMALE)
        self.jaya.add_child(drini)

        results = family_tree.find_mother_with_most_girl_child()
        self.assertEqual(Counter(results), Counter([self.jaya]))

    def test_find_relationship_between(self):
        family_tree = FamilyTree(self.king_shan)

        rel = family_tree.find_relationship_between(self.king_shan, self.satya)
        self.assertEqual(rel, "DAUGHTER")

        rel = family_tree.find_relationship_between(self.king_shan, self.chika)
        self.assertEqual(rel, "GRAND DAUGHTER")

        rel = family_tree.find_relationship_between(self.savya, self.saayan)
        self.assertEqual(rel, "BROTHER")

        rel = family_tree.find_relationship_between(self.satya, self.lika)
        self.assertEqual(rel, "SISTER IN LAW")

    def test_find_persons_from_relationship(self):
        family_tree = FamilyTree(self.king_shan)

        results = family_tree.find_persons_from_relationship(self.satvy, Relationships.PATERNAL_UNCLES)
        self.assertEqual(Counter(results), Counter([self.vich, self.chit, self.ish]))

        results = family_tree.find_persons_from_relationship(self.drita,
                                                             Relationships.PATERNAL_AUNTS)
        self.assertEqual(Counter(results),
                          Counter([self.satya, self.lika]))

        results = family_tree.find_persons_from_relationship(self.jnki,
                                                             Relationships.SISTER_IN_LAWS)
        self.assertEqual(Counter(results),
                          Counter([self.chika]))

        results = family_tree.find_persons_from_relationship(self.krpi,
                                                             Relationships.BROTHER_IN_LAWS)
        self.assertEqual(Counter(results),
                          Counter([self.saayan]))

        results = family_tree.find_persons_from_relationship(self.savya,
                                                             Relationships.COUSINS)
        self.assertEqual(Counter(results),
                          Counter([self.chika, self.vila, self.vrita, self.drita]))

        results = family_tree.find_persons_from_relationship(self.lika,
                                                             Relationships.GRAND_DAUGHTERS)
        self.assertEqual(Counter(results),
                          Counter([self.lavnya]))

        results = family_tree.find_persons_from_relationship(self.satvy,
                                                             Relationships.SIBLINGS)
        self.assertEqual(Counter(results),
                          Counter([self.savya, self.saayan]))

    def setUp(self):
        self.king_shan = Person("King Shan", Gender.MALE)
        self.queen_agna = Person("Queen Agna", Gender.FEMALE)

        self.king_shan.add_spouse(self.queen_agna)

        self.ish = Person("Ish", Gender.MALE)
        self.chit = Person("Chit", Gender.MALE)
        self.vich = Person("Vich", Gender.MALE)
        self.satya = Person("Satya", Gender.FEMALE)

        self.king_shan.add_child(self.ish)
        self.king_shan.add_child(self.chit)
        self.king_shan.add_child(self.vich)
        self.king_shan.add_child(self.satya)

        self.ambi = Person("Ambi", Gender.FEMALE)
        self.chit.add_spouse(self.ambi)

        self.lika = Person("Lika", Gender.FEMALE)
        self.vich.add_spouse(self.lika)

        self.vyan = Person("Vyan", Gender.MALE)
        self.satya.add_spouse(self.vyan)

        self.drita = Person("Drita", Gender.MALE)
        self.jaya = Person("Jaya", Gender.FEMALE)
        self.vrita = Person("Vrita", Gender.MALE)
        self.jata = Person("Jata", Gender.MALE)
        self.driya = Person("Driya", Gender.FEMALE)
        self.mnu = Person("Mnu", Gender.MALE)

        self.chit.add_child(self.drita)
        self.chit.add_child(self.vrita)
        self.drita.add_spouse(self.jaya)
        self.drita.add_child(self.jata)
        self.drita.add_child(self.driya)
        self.driya.add_spouse(self.mnu)

        self.vila = Person("Vila", Gender.MALE)
        self.jnki = Person("Jnki", Gender.FEMALE)
        self.chika = Person("Chika", Gender.FEMALE)
        self.kpila = Person("Kpila", Gender.MALE)
        self.lavnya = Person("Lavnya", Gender.FEMALE)
        self.gru = Person("Gru", Gender.MALE)

        self.vich.add_child(self.vila)
        self.vich.add_child(self.chika)
        self.vila.add_spouse(self.jnki)
        self.chika.add_spouse(self.kpila)
        self.vila.add_child(self.lavnya)
        self.lavnya.add_spouse(self.gru)

        self.satvy = Person("Satvy", Gender.FEMALE)
        self.asva = Person("Asva", Gender.MALE)
        self.savya = Person("Savya", Gender.MALE)
        self.krpi = Person("Krpi", Gender.FEMALE)
        self.saayan = Person("Saayan", Gender.MALE)
        self.mina = Person("Mina", Gender.FEMALE)
        self.kriya = Person("Kriya", Gender.MALE)
        self.misa = Person("Misa", Gender.MALE)

        self.satya.add_child(self.satvy)
        self.satya.add_child(self.savya)
        self.satya.add_child(self.saayan)
        self.satvy.add_spouse(self.asva)
        self.savya.add_spouse(self.krpi)
        self.saayan.add_spouse(self.mina)
        self.savya.add_child(self.kriya)
        self.saayan.add_child(self.misa)
