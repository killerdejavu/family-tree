from enum import Enum


class Person(object):

    def __init__(self, name, gender):
        assert gender in [Gender.MALE, Gender.FEMALE]

        self.name = name
        self.gender = gender
        self.spouse = []
        self.children = []
        self.parents = []

    def __repr__(self):
        return "{}({})".format(self.name,str.upper(self.gender.value[0]))

    def add_spouse(self, person):
        if person not in self.spouse:
            self.spouse.append(person)
        if self not in person.spouse:
            person.spouse.append(self)

    def add_child(self, person):
        if person not in self.children:
            self.children.append(person)
        spouses = self.spouse
        for spouse in spouses:
            if person not in spouse.children:
                spouse.children.append(person)

        person.parents.append(self)
        person.parents.extend(self.spouse)


class Gender(Enum):
    MALE = 'male'
    FEMALE = 'female'