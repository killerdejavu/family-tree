from family_tree.relationships import Relationships
from family_tree.family_tree import FamilyTree
from family_tree.person import Person, Gender


def main():
    king_shan = Person("King Shan", Gender.MALE)
    queen_agna = Person("Queen Agna", Gender.FEMALE)

    king_shan.add_spouse(queen_agna)
    queen_agna.add_spouse(
        king_shan)  # Not required, if would add correctly other wise as well

    ish = Person("Ish", Gender.MALE)
    chit = Person("Chit", Gender.MALE)
    vich = Person("Vich", Gender.MALE)
    satya = Person("Satya", Gender.FEMALE)

    king_shan.add_child(ish)
    king_shan.add_child(chit)
    king_shan.add_child(vich)
    king_shan.add_child(satya)

    ambi = Person("Ambi", Gender.FEMALE)
    chit.add_spouse(ambi)

    lika = Person("Lika", Gender.FEMALE)
    vich.add_spouse(lika)

    vyan = Person("Vyan", Gender.MALE)
    satya.add_spouse(vyan)

    drita = Person("Drita", Gender.MALE)
    jaya = Person("Jaya", Gender.FEMALE)
    vrita = Person("Vrita", Gender.MALE)
    jata = Person("Jata", Gender.MALE)
    driya = Person("Driya", Gender.FEMALE)
    mnu = Person("Mnu", Gender.MALE)

    chit.add_child(drita)
    chit.add_child(vrita)
    drita.add_spouse(jaya)
    drita.add_child(jata)
    drita.add_child(driya)
    driya.add_spouse(mnu)

    vila = Person("Vila", Gender.MALE)
    jnki = Person("Jnki", Gender.FEMALE)
    chika = Person("Chika", Gender.FEMALE)
    kpila = Person("Kpila", Gender.MALE)
    lavnya = Person("Lavnya", Gender.FEMALE)
    gru = Person("Gru", Gender.MALE)

    vich.add_child(vila)
    vich.add_child(chika)
    vila.add_spouse(jnki)
    chika.add_spouse(kpila)
    vila.add_child(lavnya)
    lavnya.add_spouse(gru)

    satvy = Person("Satvy", Gender.FEMALE)
    asva = Person("Asva", Gender.MALE)
    savya = Person("Savya", Gender.MALE)
    krpi = Person("Krpi", Gender.FEMALE)
    saayan = Person("Saayan", Gender.MALE)
    mina = Person("Mina", Gender.FEMALE)
    kriya = Person("Kriya", Gender.MALE)
    misa = Person("Misa", Gender.MALE)

    satya.add_child(satvy)
    satya.add_child(savya)
    satya.add_child(saayan)
    satvy.add_spouse(asva)
    savya.add_spouse(krpi)
    saayan.add_spouse(mina)
    savya.add_child(kriya)
    saayan.add_child(misa)

    print("problem 1")
    print("-"*50)
    family_tree = FamilyTree(king_shan)
    print(family_tree.find_persons_from_relationship(person=ish, relationship=Relationships.BROTHERS))
    print()

    print("problem 2")
    print("-" * 50)
    vanya = Person("vanya", gender=Gender.FEMALE)
    lavnya.add_child(vanya)
    print(family_tree.find_persons_from_relationship(person=jnki,
                                                     relationship=Relationships.GRAND_DAUGHTERS))
    print()

    print("problem 3")
    print("-" * 50)
    print(family_tree.find_mother_with_most_girl_child())
    drini = Person("Drini", gender=Gender.FEMALE)
    jaya.add_child(drini)
    print(family_tree.find_mother_with_most_girl_child())
    print()

    print("problem 4")
    print("-" * 50)
    print(family_tree.find_relationship_between(kriya, saayan))
    print()

if __name__ == '__main__':
    main()
