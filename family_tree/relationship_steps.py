from enum import Enum

from family_tree.operator import And, Or


class BaseSteps(Enum):
    PARENTS = "parents"
    MALE = "male"
    FEMALE = "female"
    CHILDREN = "children"
    SPOUSE = "spouse"


class RelationshipSteps(object):

    def __init__(self):
        pass

    parents = And(BaseSteps.PARENTS)

    father = And(BaseSteps.PARENTS,
                 BaseSteps.MALE)

    mother = And(BaseSteps.PARENTS,
                 BaseSteps.FEMALE)

    children = And(BaseSteps.CHILDREN)

    sons = And(BaseSteps.CHILDREN,
               BaseSteps.MALE)

    daughters = And(BaseSteps.CHILDREN,
                    BaseSteps.FEMALE)

    siblings = And(BaseSteps.PARENTS,
                   BaseSteps.CHILDREN)

    brothers = And(siblings, BaseSteps.MALE)

    sisters = And(siblings, BaseSteps.FEMALE)

    grand_daughters = And(BaseSteps.CHILDREN,
                          BaseSteps.CHILDREN,
                          BaseSteps.FEMALE)

    cousins = And(BaseSteps.PARENTS,
                  siblings,
                  BaseSteps.CHILDREN)

    brother_in_laws = Or(
        And(BaseSteps.SPOUSE,
            brothers),
        And(siblings,
            BaseSteps.SPOUSE,
            BaseSteps.MALE)
    )

    sister_in_laws = Or(
        And(BaseSteps.SPOUSE,
            sisters),
        And(siblings,
            BaseSteps.SPOUSE,
            BaseSteps.FEMALE)
    )

    maternal_aunts = Or(
        And(mother, sisters),
        And(mother, sister_in_laws)
    )

    paternal_aunts = Or(
        And(father, sisters),
        And(father, sister_in_laws)
    )

    maternal_uncles = Or(
        And(mother, brothers),
        And(mother, brother_in_laws)
    )

    paternal_uncles = Or(
        And(father, brothers),
        And(father, brother_in_laws)
    )