from family_tree.operator import And, Or
from family_tree.person import Gender
from family_tree.relationship_steps import BaseSteps


def process(data, steps):
    result = []

    if type(steps) == And:
        temp_result = data
        for step in steps.objects:
            temp_result = process(temp_result, step)
        result = list(filter(lambda x: x not in data, temp_result))

    if type(steps) == Or:
        temp_result = []
        for step in steps.objects:
            r = process(data, step)
            temp_result.extend(r)
        result = list(filter(lambda x: x not in data, temp_result))

    if type(steps) == BaseSteps:
        func = globals()["handle_{}".format(steps.value)]
        result = func(data)

    return result


def handle_children(data):
    children = []
    for t in data:
        children.extend(t.children)
    return list(set(children))


def handle_female(data):
    return list(filter(lambda x: x.gender == Gender.FEMALE, data))


def handle_male(data):
    return list(filter(lambda x: x.gender == Gender.MALE, data))


def handle_spouse(data):
    spouses = []
    for t in data:
        spouses.extend(t.spouse)
    return list(set(spouses))


def handle_parents(data):
    parents = []

    for t in data:
        parents.extend(t.parents)
    return list(set(parents))
